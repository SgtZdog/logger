﻿// ReSharper disable All

namespace Logger
{
    using System;
    using System.IO;

    public static class LogWriter
    {
        private const string ErrorFile = "ErrorLog.txt";
        private const string MessageFile = "MessageLog.txt";

        /// <summary>
        ///     Takes in message strings, an exception and a file path and writes to the set filepath. All values are optional.
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="filePath"></param>
        /// <param name="messages"></param>
        public static void WriteToLog(Exception exception = null, string filePath = ErrorFile, params string[] messages)
        {
            //If there is no exception and no file path, write to the default message log
            if (ErrorFile == filePath && null == exception)
            {
                filePath = MessageFile;
            }

            //Create the message string to print
            string message = LogMessageCreator(exception, messages);

            //Write message to the specified filePath
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(filePath, true))
                {
                    streamWriter.WriteLine(message);
                }
            }
            //If it fails, write to the default errorFile path, including an error message about the previous failure
            catch (Exception e)
            {
                try
                {
                    WriteToLog(e, filePath: ErrorFile);
                    WriteToLog(filePath: ErrorFile, messages: message);
                }
                //If even that fails, try writing the original message to the console. If this fails, will result in CTD
                catch
                {
                    Console.WriteLine("Failed to write to file.");
                    Console.WriteLine(message);
                }
            }
        }

        /// <summary>
        ///     Create message string by concanting individual strings together
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="inputsStrings"></param>
        /// <returns></returns>
        private static string LogMessageCreator(Exception exception = null, params string[] inputsStrings)
        {
            string output = "\n" + DateTime.Now.ToString();
            foreach (string input in inputsStrings)
            {
                output += "\n   " + input;
            }

            if (null != exception)
            {
                foreach (var key in exception.Data.Keys)
                {
                    output += "\n   " + key + ": " + exception.Data[key];
                }

                output += "\n   " + exception.ToString();
            }

            return output;
        }
    }
}